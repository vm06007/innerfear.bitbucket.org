/**
 * Created by 23rd and Walnut
 * www.23andwalnut.com
 * User: Saleem El-Amin
 * Date: 6/8/11
 * Time: 9:39 AM
 */

var myPlaylist = [

    {
        mp3:'player/mix/1.mp3',
        oga:'player/mix/1.ogg',
        title:'Sample',
        artist:'Sample',
        rating:4,
        buy:'#',
        price:'0.99',
        duration:'0:30',
        cover:'player/mix/1.png'
    },
    
    
        {
        mp3:'player/mix/1.mp3',
        oga:'player/mix/1.ogg',
        title:'Sample2',
        artist:'Sample2',
        rating:4,
        buy:'#',
        price:'0.99',
        duration:'0:30',
        cover:'player/mix/1.png'
    },
    
    
        {
        mp3:'player/mix/1.mp3',
        oga:'player/mix/1.ogg',
        title:'Sample3',
        artist:'Sample3',
        rating:4,
        buy:'#',
        price:'0.99',
        duration:'0:30',
        cover:'player/mix/1.png'
    },
    
        {
        mp3:'player/mix/1.mp3',
        oga:'player/mix/1.ogg',
        title:'Sample4',
        artist:'Sample4',
        rating:4,
        buy:'#',
        price:'0.99',
        duration:'0:30',
        cover:'player/mix/1.png'
    }
];
