                <p id="lyric1" style="display:none" class="font1 pad_bot1">
                
                "Ride to the silent shores and fly through the gardens of time
                <br/>You will find your heaven
                <br/>Your god had closed his eyes
                <br/>Alone, free you are"
                <br/>
                <br/>Juhani Palomäki 1997, the Year of Inner Fear ::: Forever Yours Marthus & Khopec - RIP Juhani
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                </p>

      <!-- ++++++++++++++++++ FEAR PROCLAIMED ++++++++++++++++++++  -->
                				
                <p id="lyric2" class="font1 pad_bot1">
                                                                                 
                As night replaces day and wonders start their absolution<br/>
                Rises deconstructive and inevitable lust<br/>
                Expect no joy and pleasure, one awaits the other measure<br/>
                From the knowledge of my generated past<br/>                     
                <br/>
                Righteous! Out of my eyes to see one<br/>
                Spirit! Deep in my flesh to feel one<br/>
                Wonders! Something is out there, I’m for, now
                <br/><br/>
                When whispers trying to rebound<br/>
                You hear the colours, see the sounds at once<br/>
                The trembling does your soul apart<br/>
                They’ll never get my inner art foreseen<br/>
                <br/>
                Lepers with halos<br/> 
                Lepers with halos<br/><br/>
                I must enthrone myself through burden of the years<br/>
                Lepers with halos Lepers with halos<br/>
                When the sadness has to deal with other deeds
                <br/><br/>
                For I am nothing but inventor of my shell<br/>
                For Burden of grief is so fragile to re-burn the hell<br/>
                For I exist, hail the fear proclamation<br/>
                <br/>
                Righteous! Telling the lies to be one<br/>
                Spirit! Never allow one being gone<br/>
                Wonders! Something is out there, I’m for, now<br/>
                <br/>
                And whispers trying to rebound<br/>
                You hear the colours, see the sounds at once<br/>
                The trembling does your soul apart<br/>
                They’ll never get my inner art foreseen<br/>
                <br/>
                For I’ll do nothing to destroy that perfect shell<br/>
                For Burden of grief is so fragile to re-burn the hell<br/>
                For I exist, hail the fear proclamation<br/>
                <br/>
                It’s done by now! Apparently, sleeping<br/>
                Rise up like the morning without the dawn<br/>
                Just breathe somehow and your heart could start beating<br/>
                Shine on like the glory without the sun<br/>
                
                </p>     

                <!-- ++++++++++++++++++ IMPRISONED IN FORGOTTEN DUNGEON ++++++++++++++++++++  -->
                
                <p id="lyric3" style="display:none" class="font1 pad_bot1">
                                                                                                 
                When light is melting till last solid seed
                <br/>And shadows start to reign my tiny world
                <br/><br/>
                Waiting for the splendid horror
                <br/>Sacrificing consciousness I bear
                <br/>I don’t need no fading world
                <br/>Just visions, whispers in the air
                <br/><br/>
                They unite, unveiling the darkness
                <br/>Acting so perfectly, feeding me well
                <br/>Suddenly I’m comprehending the reason
                <br/>Reason I’m left here to rot in this hell
                <br/><br/>
                And only shadows reign my tiny world
                <br/>My restless dreams embracing them again
                <br/><br/>
                Waiting for the splendid horror
                <br/>Dignity I’ve buried deep inside
                <br/>I don’t need no false seduction
                <br/>Devastating slowly in my mind
                <br/><br/>
                For the emptiness I’m conceived
                <br/><br/>
                Watching their creepy elaborate art
                <br/>I’m torn asunder in motion
                <br/>Over and over acquiring me
                <br/>Collapsing the chances for me being free...
                <br/><br/>
                Once you are trapped
                <br/>In such a deep and restless story
                <br/>You will never forget
                <br/>What it means collapsing for it
                <br/><br/>
                Imprisoned in forgotten dungeon
                <br/>Never forget
                <br/>I’m imprisoned in forgotten dungeon 
                
                </p>
                   
                <!-- ++++++++++++++++++ LUSTMISTRESS ++++++++++++++++++++  -->                   
                   
                <p id="lyric4" style="display:none" class="font1 pad_bot1">
                                                                                                 
                Knee my life to thee
                <br/>I’m thy trembling lord of trivial
                <br/>Rip my heart of me
                <br/>The signs of love no longer needed
                <br/>Torn me into dust
                <br/>The wind will catch my sense of living
                <br/>Everlasting lust
                <br/>The only air I’m merely breathing
                <br/>
                <br/>Nailed will with your body’s thrill
                <br/>Burning ground, fever that will kill
                <br/>
                <br/>In her mighty affection I’m drown
                <br/>Even shadows are afraid of her spawn
                <br/>She rules her selfishness at once
                <br/>
                <br/>Undressed to use my spirit
                <br/>Her rigid passion heals it
                <br/>I’m crumbing down when she’s above
                <br/>
                <br/>Knee my life to thee
                <br/>I’m thy trembling lord of trivial
                <br/>Rip my heart of me
                <br/>The signs of love no longer needed
                <br/>Torn me into dust
                <br/>The wind will catch my sense of living
                <br/>Everlasting lust
                <br/>The only air I’m merely breathing
                <br/>
                <br/>In her mighty affection I’m drown
                <br/>Even shadows are afraid of her spawn
                <br/>My precious flesh’s no longer mine
                <br/>
                <br/>Undressed to rape my spirit
                <br/>Her rigid passion heals it
                <br/>Her body’s covered with divine
                
                </p>   
                
                <!-- ++++++++++++++++++ I WATCH THE BLOOD FOREVER ++++++++++++++++++++  -->                
                
                <p id="lyric5" style="display:none" class="font1 pad_bot1">
                                                                                                 
                Nothingness, finally, turned to obsessive belief
                <br/>
                <br/>Flashing lights
                <br/>The impulse strikes the frames again
                <br/>Empty sights
                <br/>Reanimating dreams away
                <br/>My only patterns are the thoughts I ruin by myself
                <br/>And watching blood – the motion that my soul obey
                <br/>
                <br/>Wake the disease
                <br/>It doesn’t hurt, the cure is so despiteful
                <br/>Take it with ease
                <br/>
                <br/>But preeminent agony - icons to pray
                <br/>Send me the image before I awake
                <br/>
                <br/>Endless pain
                <br/>For I’ve been struggling through my way back home
                <br/>In my veins
                <br/>I can’t deny the blessings overcome
                <br/>Crawling shadows are the beings we’re never able to detach
                <br/>Watching blood - the pleasure I adore too much
                <br/>
                <br/>Wake the disease
                <br/>It doesn’t hurt, the cure is so despiteful
                Take it with ease
                <br/>
                <br/>And preeminent agony, icons to pray
                <br/>Send me the image before it’s too late
                <br/>
                <br/>Nothingness, finally turned
                <br/>
                <br/>It’s just a dream but I’m over you
                <br/>Believing's so useless
                <br/>Betrayal vision’s not coming true
                <br/>Denial between us
                <br/>
                <br/>These wounds you’ve granted me would not sustain the fight
                <br/>Deprave my vision, while the script is staying even
                <br/>Like inspiration fading, rising, coincide
                <br/>They always let me breathe for living
                <br/>
                <br/>It doesn’t hurt’ the cure is so despiteful
                <br/>Take it with ease
                <br/>
                <br/>And preeminent agony, icons to pray
                <br/>Send me the image before I awake
                
                </p>
                   
                <!-- ++++++++++++++++++ INNER FEAR ++++++++++++++++++++  -->
                                 
                <p id="lyric6" style="display:none" class="font1 pad_bot1">
                                                                                                 
                My ability to clarify your hidden world
                <br/>Never lets you feel addicted to the brave
                <br/>Just another isolated psychic wisdom falls
                <br/>Digging slowly perfect shelter for your grave
                <br/>    
                <br/>I am the perfectness of lies
                <br/>Incarnated hell, rotting paradise
                <br/>
                <br/>Drawing out attention from your ego-soul
                <br/>Lighting up the promises of mine
                <br/>Breathing desperation, frightening deception
                <br/>Leaving all your fantasies behind
                <br/>
                <br/>I’m all that scares the world
                <br/>Adoption that brings anemia to Earth
                <br/>
                <br/>I am the paradox of life
                <br/>Incarnated hell, rotting paradise
                <br/>
                <br/>Never find me hiding out away of grip
                <br/>Inspiration for your shattered falling grace
                <br/>Like the nightmare killing slowly your domain of sleep
                <br/>Terrified emotions – make up for your face
                <br/>
                <br/>Are you able to expose your inner fear?
                <br/>
                <br/>Live for me, why don’t you see?
                <br/>My presence’s so despised
                <br/>Always feel that rush of me
                <br/>I’ll crush your heart, being hypnotized
                
                </p>                     

                <!-- ++++++++++++++++++ LOVE IS ++++++++++++++++++++  -->
                 
                <p id="lyric7" style="display:none" class="font1 pad_bot1">
                                                                                                 
                They say it’s just a rapid strike
                <br/>Like the pain of burning
                <br/>Its venomous affection kills with the tongues of loving
                <br/>
                <br/>The pulse of your heart’s just a waste of time
                <br/>Eternity was supposed to be mine
                <br/>Adorable creature that never exists
                <br/>Blind fold of devotion erases its sins
                <br/>
                <br/>My venom like heaven covers you
                <br/>So deep, beneath my flesh
                <br/>
                <br/>It’s joining the feast; it’s so starving to death
                <br/>Incredible lies cuts your very last breath
                <br/>It’s like autumn that slightly absorbing your colors of dreams
                <br/>
                <br/>You can’t just imagine its pretty disguise
                <br/>It sees and it listens to the pain in your eyes
                <br/>And there’s nothing that could violate the obsession to stop
                <br/>
                <br/>They say it’s just a moment of joy
                <br/>When the time stops running
                <br/>Its merciless destruction crawls by the pawns of loving
                <br/>
                <br/>And never gonna give you a chance
                <br/><br/>For your soul’s your burden
                <br/>Ironic evolution falls recreating nothing
                <br/>
                <br/>You can’t just imagine its pretty disguise
                <br/>It sees and it listens to the pain in your eyes
                <br/>And there’s nothing that could violate the obsession to stop
                <br/>
                <br/>It’s joining the feast, it’s starving to death
                <br/>Incredible lies cuts your very last breath
                <br/>It’s like autumn that slightly absorbing your colors of dreams
                <br/>
                <br/>Adorable creature that never existed 
                <br/>Blindfold of devotion erases its sins
                <br/>
                <br/>They say it’s just an innocent flight
                <br/>But your wings are burning
                <br/>Addiction to the savior-light still exists in mourning
                <br/>
                <br/>And never gonna let you arise
                <br/>Upon your meaningless glory
                <br/>Love is a poisonous cunt
                <br/>The end of story
                <br/>
                <br/>Love is a poisonous cunt
                
                </p>

                <!-- ++++++++++++++++++ OUR CRIMSON DEEDS ++++++++++++++++++++  -->
                
                <p id="lyric8" style="display:none" class="font1 pad_bot1">
                                                                                                 
                Never ever
                <br/>Convince my soul’s endeavor
                <br/>I’m dreaming of the falling from grace
                <br/>
                <br/>One tries the weakness coming forth
                <br/>Our sickness begs for more
                <br/>
                <br/>Greed, lies of the wise
                <br/>I swear to God, but is it here?!
                <br/>Now, claim your prize
                <br/>From light we hide our inner...
                <br/>
                <br/>For the things we’ve done
                <br/>The flesh won’t cover anyone
                <br/>
                <br/>Greed, lies of the wise
                <br/>What sorrow brings this moment?!
                <br/>I’m condemned, inhumanized
                <br/>We breathe each other’s torments
                <br/>
                <br/>Find it back
                <br/>We’re trying to remember what it means
                <br/>
                <br/>As world comes tearing apart
                <br/>We’re put to the test; we’re abused by the hollow
                <br/>Our bleeding is near
                <br/>Bleeding is near
                <br/>
                <br/>Never ever
                <br/>Convince my soul’s Endeavour
                <br/>I’m leaving for the promise of faith
                <br/>
                <br/>For the things we’ve done
                <br/>The flesh won’t cover anyone
                <br/>
                <br/>Find it back
                <br/>We’re trying to remember how it feels
                <br/>
                <br/>I saw world tearing apart
                <br/>We’re put to the test, we’re abused by the hollow
                <br/>Our ending is near
                <br/>Ending is near
                
                </p>  
               
                <!-- ++++++++++++++++++ AKHU ++++++++++++++++++++  -->               
                 
                <p id="lyric9" style="display:none" class="font1 pad_bot1">
                                                                                                 
                Out of the innocent sanity
                <br/>Rushed out the widely, precious, entwined
                <br/>Oh, individual vanity
                <br/>Never reaches the endless of time
                <br/>
                <br/>Reason of my lessons
                <br/>Coming out for that
                <br/>Feeling in your consciousness the sense of regret
                <br/>
                <br/>And the embarrassed fall in different stream
                <br/>They’re floating anxious to the universe
                <br/>The spirit stands in front of oblivion, sinned
                <br/>It never wanders free among the rest
                <br/>
                <br/>Whispers are coming from the radiant well
                <br/>The time stops running, it abandons the dwell
                <br/>Let my newborn journey begin
                <br/>God of immortals let my name begin
                <br/>
                <br/>Reason of my lessons
                <br/>Dying second time
                <br/>Feeling in my consciousness the sense of divine
                <br/>
                <br/>And the embarrassed fall in different stream
                <br/>They’re floating anxious to the universe
                <br/>The spirit stands in front oblivion, sinned
                <br/>It never wanders free among the rest
                
                </p>   
                                 
                <!-- ++++++++++++++++++ SECRETS HOLDER ++++++++++++++++++++  -->      
                
                <p id="lyric10" style="display:none" class="font1 pad_bot1">
                                                                                                 
                We’ve lost confidence and cannot see the barriers
                <br/>Denying  knowledge of light and death
                <br/>That question “WHY?!” saves our life from deep euphoria
                <br/>Generation with a bullet in its head
                <br/>
                <br/>We burn our dreaming in hell
                <br/>
                <br/>And emptiness’s betraying me as well
                <br/>Deaf–muteness of the sanctity
                <br/>The rebellious no longer yell
                <br/>They hear the secrets of mortality
                <br/>
                <br/>That question “HOW?!” saves our life from euphoria
                <br/>Generation with a tumor in its head
                <br/>
                <br/>We stab our dreaming, we dwell
                <br/>
                <br/>And emptiness’s dividing me as well
                <br/>Deaf – muteness of the sanctity
                <br/>The rebellious no longer yell
                <br/>They know the secrets of mortality
                <br/>
                <br/>Could everyone stop witnessing despair?!
                <br/>
                <br/>We are exceptionally perfect, useless
                <br/>Trying to inhale what is obscene
                <br/>
                <br/>Refuse me thousand times
                <br/>But, where’s your truth?
                <br/>
                <br/>Out of mind
                <br/>Preaching model
                <br/>In my sights
                <br/>One step closer
                <br/>Paradise
                <br/>
                <br/>Out of mind
                <br/>Preaching model
                <br/>In my sights
                <br/>Secret’s holder
                <br/>Paradise
                <br/>
                <br/>False prophecies – foreseen and crumbled
                <br/>You will see in all of the ugliest beliefs
                <br/>
                <br/>So, emptiness’s betraying me again
                <br/>Deaf – muteness of the sanctity
                <br/>The rebellious no longer yell
                <br/>They tell the secrets of mortality
                <br/>
                <br/>Could everyone stop witnessing despair?!
                <br/>
                <br/>We are exceptionally perfect, useless
                <br/>Trying to inhale what is obscene
                <br/>
                <br/>Refuse me thousand times
                <br/>But where’s your truth?
                </p>