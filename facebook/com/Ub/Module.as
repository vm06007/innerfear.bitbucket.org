package com.Ub
{
    import flash.display.*;
    import flash.events.*;

    public class Module extends MovieClip
    {
        protected var xml:XML;
        protected var _content:MovieClip;

        public function Module()
        {
            addEventListener(Event.ADDED_TO_STAGE, this.init);
            this._content = this["_container"];
            return;
        }// end function

        protected function init(event:Event) : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, this.init);
            return;
        }// end function

        public function _hide() : void
        {
            return;
        }// end function

        public function get _xml() : XML
        {
            return this.xml;
        }// end function

        public function set _xml(param1:XML) : void
        {
            this.xml = param1;
            return;
        }// end function

    }
}
