﻿package com.Ub
{
    import br.com.stimuli.loading.*;
    import caurina.transitions.*;
    import caurina.transitions.properties.*;
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;

    public class Main extends MovieClip
    {
        public var _xml:XML;
        public var assetsLoader:BulkLoader;
        public var mainMenu:MenuContainer;
        public var contentConteiner:Sprite;
        public var contentMask:Sprite;
        public var contentQueue:BulkLoader;
        public var currentPage:int = 0;
        public var module:Module;
        public var loadSprite:Sprite;
        public var preloader:Preloader;
        public var allContainer:Sprite;

        public function Main()
        {
            ColorShortcuts.init();
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            var _loc_1:* = new URLLoader();
            _loc_1.addEventListener(Event.ADDED_TO_STAGE, this.init);
            _loc_1.load(new URLRequest("Config.xml"));
            return;
        }// end function

        public function init(event:Event) : void
        {
            this._xml = new XML(event.target.data);
            this.allContainer = new Sprite();
            stage.addChild(this.allContainer);
            this.allContainer.alpha = 0;
            this.loadAssets();
            return;
        }// end function

        public function loadAssets() : void
        {
            this.preloader = new Preloader();
            stage.addChild(this.preloader);
            this.preloader.x = stage.stageWidth / 2;
            this.preloader.y = stage.stageHeight / 2;
            this.assetsLoader = new BulkLoader();
            this.assetsLoader.addEventListener(BulkProgressEvent.COMPLETE, this.onAssetsComplete);
            var _loc_1:* = new URLRequest(this._xml.Bg.@source);
            this.assetsLoader.add(_loc_1, {id:"bg"});
            var _loc_2:* = new URLRequest(this._xml.Logo.@source);
            this.assetsLoader.add(_loc_2, {id:"logo"});
            this.assetsLoader.start();
            return;
        }// end function

        public function onAssetsComplete(event:BulkProgressEvent) : void
        {
            this.preloader.visible = false;
            stage.removeChild(this.preloader);
            var _loc_2:* = this.assetsLoader.get("bg").content as Bitmap;
            this.allContainer.addChildAt(_loc_2, 0);
            var _loc_3:* = this.assetsLoader.get("logo").content as Bitmap;
            _loc_3.x = this._xml.Settings.@logoXpos;
            _loc_3.y = this._xml.Settings.@logoYpos;
            this.allContainer.addChild(_loc_3);
            this.contentConteiner = new Sprite();
            this.allContainer.addChild(this.contentConteiner);
            this.contentMask = new Sprite();
            this.contentMask.graphics.beginFill(65280, 1);
            this.contentMask.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            this.contentMask.graphics.endFill();
            this.allContainer.addChild(this.contentMask);
            this.contentConteiner.mask = this.contentMask;
            this.loadSprite = new Sprite();
            this.loadSprite.graphics.beginFill(uint(this._xml.Settings.@preloaderColor));
            this.loadSprite.graphics.drawRect(0, 0, stage.stageWidth, 5);
            this.loadSprite.graphics.endFill();
            stage.addChild(this.loadSprite);
            this.loadSprite.alpha = 0;
            this.loadSprite.visible = false;
            this.makeMenu();
            this._FirstOpen();
            Tweener.addTween(this.allContainer, {time:1, alpha:1});
            return;
        }// end function

        public function makeMenu() : void
        {
            this.mainMenu = new MenuContainer(this._xml);
            this.mainMenu.x = Number(this._xml.Settings.@menuXpos);
            this.mainMenu.y = Number(this._xml.Settings.@menuYpos);
            this.mainMenu._bgColor = this._xml.Settings.@menuItem_bg_color;
            this.mainMenu._outColor = this._xml.Settings.@menuItem_text_color;
            this.mainMenu._overColor = this._xml.Settings.@menuItem_text_color_selected;
            this.allContainer.addChildAt(this.mainMenu, stage.numChildren);
            this.mainMenu.addEventListener(MenuContainer.MENU_CLICKED, this.onMenuClick);
            return;
        }// end function

        public function _FirstOpen() : void
        {
            this.mainMenu._selectFirstItem();
            return;
        }// end function

        public function onMenuClick(event:Event) : void
        {
            var _loc_4:String = null;
            var _loc_5:URLRequest = null;
            var _loc_6:URLRequest = null;
            var _loc_2:* = this.mainMenu._moduleType;
            var _loc_3:* = this.mainMenu._moduleXml;
            switch(_loc_2)
            {
                case "music":
                {
                    _loc_4 = "MusicModule.swf";
                    break;
                }
                case "text":
                {
                    _loc_4 = "TextModule.swf";
                    break;
                }
                case "news":
                {
                    _loc_4 = "NewsModule.swf";
                    break;
                }
                case "gallery":
                {
                    _loc_4 = "GalleryModule.swf";
                    break;
                }
                case "youtube":
                {
                    _loc_4 = "YoutubeModule.swf";
                    break;
                }
                case "contact":
                {
                    _loc_4 = "ContactModule.swf";
                    break;
                }
                default:
                {
                    break;
                }
            }
            this.contentQueue = new BulkLoader();
            this.contentQueue.addEventListener(BulkProgressEvent.PROGRESS, this.onContentProgress);
            this.contentQueue.addEventListener(BulkProgressEvent.COMPLETE, this.onContentLoaded);
            _loc_5 = new URLRequest(_loc_4);
            _loc_6 = new URLRequest(_loc_3);
            this.contentQueue.add(_loc_5, {id:"Mc_id"});
            this.contentQueue.add(_loc_6, {id:"Xml_id"});
            this.contentQueue.start();
            this.loadSprite.x = 0;
            this.loadSprite.scaleX = 0;
            this.loadSprite.visible = true;
            this.loadSprite.alpha = 1;
            return;
        }// end function

        public function onContentProgress(event:BulkProgressEvent) : void
        {
            this.loadSprite.scaleX = event.target.percentLoaded;
            return;
        }// end function

        public function onContentLoaded(event:BulkProgressEvent) : void
        {
            var xml:XML;
            var content:Module;
            var e:* = event;
            Tweener.addTween(this.loadSprite, {time:1, 450, scaleX:0, alpha:0, onComplete:function () : void
            {
                loadSprite.visible = false;
                return;
            }// end function
            });
            xml = this.contentQueue.getXML("Xml_id");
            content = this.contentQueue.get("Mc_id").content as Module;
            if (this.contentConteiner.numChildren != 0)
            {
                (this.contentConteiner.getChildAt(0) as Module)._hide();
                Tweener.addTween(this.contentMask, {time:1, y:-stage.stageHeight, onComplete:function () : void
            {
                contentConteiner.removeChildAt(0);
                content._xml = xml;
                contentConteiner.addChild(content);
                Tweener.addTween(contentMask, {time:1, y:0});
                return;
            }// end function
            });
            }
            else
            {
                Tweener.addTween(this.contentMask, {y:-stage.stageHeight});
                content._xml = xml;
                this.contentConteiner.addChild(content);
                Tweener.addTween(this.contentMask, {time:1, y:0});
            }
            return;
        }// end function

    }
}
