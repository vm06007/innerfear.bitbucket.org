package com.Ub
{
    import caurina.transitions.properties.*;
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;

    public class MenuItem extends MovieClip
    {
        public var sp:MovieClip;
        public var bg:MovieClip;
        public var tf:TextField;
        private var id:int;
        private var content:String;
        private var type:String;
        private var selected:Boolean;

        public function MenuItem(param1:String, param2:String, param3:String)
        {
            ColorShortcuts.init();
            this.type = param2;
            this.content = param3;
            this.tf.text = param1;
            addEventListener(Event.ADDED_TO_STAGE, this.init);
            this.tf.autoSize = TextFieldAutoSize.LEFT;
            this.sp.width = this.tf.width + 4;
            this.bg.width = this.tf.width + 4;
            this.sp.x = -2;
            this.bg.x = -2;
            return;
        }// end function

        private function init(event:Event) : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, this.init);
            this.sp.scaleY = 0;
            this.bg.scaleY = 0;
            var _loc_2:Boolean = true;
            this.buttonMode = true;
            this.useHandCursor = _loc_2;
            this.mouseChildren = false;
            return;
        }// end function

        public function get _type() : String
        {
            return this.type;
        }// end function

        public function get _content() : String
        {
            return this.content;
        }// end function

        public function get _selected() : Boolean
        {
            return this.selected;
        }// end function

        public function set _selected(param1:Boolean) : void
        {
            this.selected = param1;
            return;
        }// end function

        public function get _id() : int
        {
            return this.id;
        }// end function

        public function set _id(param1:int) : void
        {
            this.id = param1;
            return;
        }// end function

    }
}
