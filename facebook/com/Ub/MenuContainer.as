package com.Ub
{
    import caurina.transitions.*;
    import caurina.transitions.properties.*;
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;

    public class MenuContainer extends MovieClip
    {
        private var _xml:XML;
        private var miArray:Array;
        private var menuX:Number = 0;
        private var outColor:String = "0xffffff";
        private var overColor:String = "0xff9900";
        private var bgColor:String = "0x000000";
        private var currItem:int = 0;
        private var nextItem:int = 0;
        private var moduleType:String;
        private var moduleXml:String;
        public static const MENU_CLICKED:String = "menu clicked";

        public function MenuContainer(param1:XML)
        {
            ColorShortcuts.init();
            this._xml = new XML(param1);
            this.init();
            return;
        }// end function

        private function init() : void
        {
            var _loc_2:MenuItem = null;
            this.miArray = new Array();
            var _loc_1:int = 0;
            while (_loc_1 < this._xml.Pages.item.length())
            {
                
                _loc_2 = new MenuItem(this._xml.Pages.item[_loc_1].@title, this._xml.Pages.item[_loc_1].@type, this._xml.Pages.item[_loc_1].@content);
                Tweener.addTween(_loc_2.sp, {_color:this.bgColor});
                Tweener.addTween(_loc_2.tf, {time:0.3, _color:this.outColor, transition:"easeOutQuint"});
                _loc_2.addEventListener(MouseEvent.MOUSE_OUT, this.onMenuOut);
                _loc_2.addEventListener(MouseEvent.MOUSE_OVER, this.onMenuOver);
                _loc_2.addEventListener(MouseEvent.CLICK, this.onMenuClick);
                _loc_2.x = this.menuX;
                _loc_2._id = _loc_1;
                this.menuX = this.menuX + _loc_2.width;
                this.addChild(_loc_2);
                this.miArray.push(_loc_2);
                _loc_1++;
            }
            return;
        }// end function

        private function onMenuOver(event:MouseEvent) : void
        {
            Tweener.addTween(event.target.tf, {time:0.3, _color:this.overColor, transition:"easeOutQuint"});
            return;
        }// end function

        private function onMenuOut(event:MouseEvent) : void
        {
            Tweener.addTween(event.target.tf, {time:0.3, _color:this.outColor, transition:"easeOutQuint"});
            Tweener.addTween(event.target.sp, {time:0.3, scaleY:0, transition:"easeOutQuint"});
            return;
        }// end function

        private function onMenuClick(event:MouseEvent) : void
        {
            var _loc_3:MenuItem = null;
            this.nextItem = event.currentTarget._id;
            var _loc_2:int = 0;
            while (_loc_2 < this.miArray.length)
            {
                
                _loc_3 = this.miArray[_loc_2];
                if (_loc_3._id != this.nextItem)
                {
                    _loc_3._selected = false;
                    _loc_3.addEventListener(MouseEvent.MOUSE_OUT, this.onMenuOut);
                    _loc_3.addEventListener(MouseEvent.CLICK, this.onMenuClick);
                    Tweener.addTween(_loc_3.tf, {time:0.5, _color:this.outColor, transition:"easeOutQuint"});
                    Tweener.addTween(_loc_3.sp, {time:0.5, scaleY:0, transition:"easeOutQuint"});
                }
                if (_loc_3._id == this.nextItem)
                {
                    if (_loc_3._type == "link")
                    {
                        navigateToURL(new URLRequest(_loc_3._content), "_blank");
                    }
                    else
                    {
                        _loc_3._selected = true;
                        this.currItem = this.nextItem;
                        Tweener.addTween(event.target.sp, {time:0.3, scaleY:1, transition:"easeOutQuint"});
                        _loc_3.removeEventListener(MouseEvent.MOUSE_OUT, this.onMenuOut);
                        _loc_3.removeEventListener(MouseEvent.CLICK, this.onMenuClick);
                        this.moduleType = _loc_3._type;
                        this.moduleXml = _loc_3._content;
                        dispatchEvent(new Event(MENU_CLICKED));
                    }
                }
                _loc_2++;
            }
            return;
        }// end function

        public function _selectFirstItem() : void
        {
            var _loc_1:* = this.miArray[this.currItem];
            if (_loc_1._type == "link")
            {
                navigateToURL(new URLRequest(_loc_1._content), "_blank");
            }
            else
            {
                _loc_1._selected = true;
                Tweener.addTween(_loc_1.sp, {time:0.3, scaleY:1, transition:"easeOutQuint"});
                _loc_1.removeEventListener(MouseEvent.MOUSE_OUT, this.onMenuOut);
                _loc_1.removeEventListener(MouseEvent.CLICK, this.onMenuClick);
                this.moduleType = _loc_1._type;
                this.moduleXml = _loc_1._content;
                dispatchEvent(new Event(MENU_CLICKED));
            }
            return;
        }// end function

        public function get _moduleType() : String
        {
            return this.moduleType;
        }// end function

        public function get _moduleXml() : String
        {
            return this.moduleXml;
        }// end function

        public function set _outColor(param1:String) : void
        {
            this.outColor = param1;
            return;
        }// end function

        public function set _overColor(param1:String) : void
        {
            this.overColor = param1;
            return;
        }// end function

        public function set _bgColor(param1:String) : void
        {
            this.bgColor = param1;
            return;
        }// end function

    }
}
