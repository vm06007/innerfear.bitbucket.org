package com.Ub.Music
{
    import caurina.transitions.*;
    import caurina.transitions.properties.*;
    import flash.display.*;
    import flash.events.*;
    import flash.media.*;
    import flash.net.*;
    import flash.text.*;
    import flash.utils.*;

    public class Play extends MusicCore
    {
        public var title_tf:TextField;
        public var link_btn:MovieClip;
        public var controls:MovieClip;
        public var album_tf:TextField;
        private var songList:XMLList;
        private var volumeVal:Number = 1;
        private var song:Sound;
        private var songCh:SoundChannel;
        private var paused:Boolean;
        private var pauseTime:Number;
        private var prevBtn:Sprite;
        private var nextBtn:Sprite;
        private var playBtn:Sprite;
        private var muted:Boolean = false;
        private var txt:String = "";
        private var tf:TextField;
        private var newTxt:String = "";
        private var i:Number = 0;
        private var timer:Timer;
        public static const SONG_CHANGED:String = "song changed";

        public function Play()
        {
            return;
        }// end function

        override protected function init(event:Event) : void
        {
            ColorShortcuts.init();
            removeEventListener(Event.ADDED_TO_STAGE, this.init);
            this.song = new Sound();
            this.prevBtn = this.controls.prev_btn;
            this.nextBtn = this.controls.next_btn;
            this.playBtn = this.controls.play_btn;
            var _loc_2:Boolean = true;
            this.link_btn.buttonMode = true;
            var _loc_2:* = _loc_2;
            this.controls.progress_bar.buttonMode = _loc_2;
            var _loc_2:* = _loc_2;
            this.playBtn.buttonMode = _loc_2;
            var _loc_2:* = _loc_2;
            this.nextBtn.buttonMode = _loc_2;
            this.prevBtn.buttonMode = _loc_2;
            var _loc_2:Boolean = true;
            this.link_btn.useHandCursor = true;
            var _loc_2:* = _loc_2;
            this.controls.progress_bar.useHandCursor = _loc_2;
            var _loc_2:* = _loc_2;
            this.playBtn.useHandCursor = _loc_2;
            var _loc_2:* = _loc_2;
            this.nextBtn.useHandCursor = _loc_2;
            this.prevBtn.useHandCursor = _loc_2;
            this.link_btn.mouseChildren = false;
            stage.addEventListener(Event.ENTER_FRAME, this.updateSongTime);
            this.colorThings();
            var _loc_2:Boolean = true;
            this.controls.vol_icon.buttonMode = true;
            this.controls.volume_bar.buttonMode = _loc_2;
            var _loc_2:Boolean = true;
            this.controls.vol_icon.useHandCursor = true;
            this.controls.volume_bar.useHandCursor = _loc_2;
            this.controls.volume_bar.addEventListener(MouseEvent.CLICK, this.onVolumeClick);
            this.controls.vol_icon.addEventListener(MouseEvent.CLICK, this.onVolBtnClick);
            this.controls.progress_bar.addEventListener(MouseEvent.CLICK, this.onSeekBarClick);
            this.prevBtn.addEventListener(MouseEvent.CLICK, this.onPrevBtnClick);
            this.nextBtn.addEventListener(MouseEvent.CLICK, this.onNextBtnClick);
            this.playBtn.addEventListener(MouseEvent.CLICK, this.onPlayBtnClick);
            this.prevBtn.addEventListener(MouseEvent.MOUSE_OVER, this.onBtnOver);
            this.nextBtn.addEventListener(MouseEvent.MOUSE_OVER, this.onBtnOver);
            this.playBtn.addEventListener(MouseEvent.MOUSE_OVER, this.onBtnOver);
            this.prevBtn.addEventListener(MouseEvent.MOUSE_OUT, this.onBtnOut);
            this.nextBtn.addEventListener(MouseEvent.MOUSE_OUT, this.onBtnOut);
            this.playBtn.addEventListener(MouseEvent.MOUSE_OUT, this.onBtnOut);
            this.controls.play_btn.gotoAndStop(1);
            this.controls.vol_icon.gotoAndStop(1);
            return;
        }// end function

        override public function PLAY_ON() : void
        {
            this._reveal();
            this.x = 220;
            this.y = 100;
            return;
        }// end function

        override protected function _reveal() : void
        {
            this.alpha = 0;
            visible = true;
            Tweener.addTween(this, {delay:1, time:1, alpha:1});
            return;
        }// end function

        public function playSong(param1:Number) : void
        {
            var val:* = param1;
            _currentSong = val;
            var newSong:* = new Sound(new URLRequest(this.songList[_currentSong].@source));
            if (this.songCh)
            {
                this.songCh.stop();
                try
                {
                    this.song.close();
                }
                catch (e:Error)
                {
                }
                this.songCh.removeEventListener(Event.SOUND_COMPLETE, this.onSongComplete);
                this.song.removeEventListener(IOErrorEvent.IO_ERROR, this.ioError);
                this.song.removeEventListener(IOErrorEvent.NETWORK_ERROR, this.ioError);
                this.song.removeEventListener(IOErrorEvent.DISK_ERROR, this.ioError);
                this.song.removeEventListener(IOErrorEvent.VERIFY_ERROR, this.ioError);
            }
            this.song = newSong;
            this.paused = false;
            this.songCh = this.song.play();
            this.controls.vol_icon.gotoAndStop(1);
            Tweener.addTween(this.controls.volume_bar.sp, {time:0.3, scaleX:this.volumeVal});
            this.muted = false;
            this.songCh.soundTransform = new SoundTransform(this.volumeVal);
            this.updateTitleAndLink();
            this.songCh.addEventListener(Event.SOUND_COMPLETE, this.onSongComplete);
            this.song.addEventListener(IOErrorEvent.IO_ERROR, this.ioError);
            this.song.addEventListener(IOErrorEvent.NETWORK_ERROR, this.ioError);
            this.song.addEventListener(IOErrorEvent.DISK_ERROR, this.ioError);
            this.song.addEventListener(IOErrorEvent.VERIFY_ERROR, this.ioError);
            dispatchEvent(new Event(SONG_CHANGED));
            return;
        }// end function

        private function ioError(event:IOErrorEvent) : void
        {
            trace("ioError");
            return;
        }// end function

        private function onSongComplete(event:Event) : void
        {
            var _loc_2:* = _currentSong + 1;
            if (_loc_2 < this.songList.length())
            {
                _currentSong = _loc_2;
                this.playSong(_loc_2);
            }
            return;
        }// end function

        private function updateSongTime(event:Event) : void
        {
            if (this.songCh)
            {
                this.controls.time_tf.text = this.formatTime(this.currentTime()) + "/ " + this.formatTime(this.duration());
                this.controls.progress_bar.sp.scaleX = this.currentTime() / this.duration();
            }
            return;
        }// end function

        public function _playNewAlbum() : void
        {
            this.songList = new XMLList(_xml.Album[_currentAlbum].song);
            this.playSong(0);
            return;
        }// end function

        private function onPlayBtnClick(event:MouseEvent) : void
        {
            if (this.songCh)
            {
                this.songCh.removeEventListener(Event.SOUND_COMPLETE, this.onSongComplete);
                if (this.paused)
                {
                    this.songCh = this.song.play(this.pauseTime);
                    this.paused = false;
                    this.controls.play_btn.gotoAndStop(1);
                }
                else
                {
                    this.pauseTime = this.songCh.position;
                    this.songCh.stop();
                    this.paused = true;
                    this.controls.play_btn.gotoAndStop(2);
                }
                this.songCh.soundTransform = new SoundTransform(this.volumeVal);
                this.songCh.addEventListener(Event.SOUND_COMPLETE, this.onSongComplete);
            }
            return;
        }// end function

        private function onPrevBtnClick(event:MouseEvent) : void
        {
            var _loc_2:* = _currentSong - 1;
            if (_loc_2 >= 0)
            {
                _currentSong = _loc_2;
                this.playSong(_loc_2);
                this.controls.play_btn.gotoAndStop(1);
            }
            return;
        }// end function

        private function onNextBtnClick(event:MouseEvent) : void
        {
            var _loc_2:* = _currentSong + 1;
            if (_loc_2 < this.songList.length())
            {
                _currentSong = _loc_2;
                this.playSong(_loc_2);
                this.controls.play_btn.gotoAndStop(1);
            }
            return;
        }// end function

        private function onSeekBarClick(event:MouseEvent) : void
        {
            var _loc_2:Number = NaN;
            if (this.songCh)
            {
                this.songCh.removeEventListener(Event.SOUND_COMPLETE, this.onSongComplete);
                _loc_2 = this.duration() * this.controls.progress_bar.mouseX / this.controls.progress_bar.width;
                this.songCh.stop();
                this.songCh = this.song.play(_loc_2);
                if (this.paused)
                {
                    this.paused = false;
                }
                this.songCh.soundTransform = new SoundTransform(this.volumeVal);
                this.songCh.addEventListener(Event.SOUND_COMPLETE, this.onSongComplete);
                this.controls.play_btn.gotoAndStop(1);
            }
            return;
        }// end function

        private function onVolumeClick(event:MouseEvent) : void
        {
            this.controls.vol_icon.gotoAndStop(1);
            this.volumeVal = this.controls.volume_bar.mouseX / this.controls.volume_bar.width;
            if (this.songCh)
            {
                this.songCh.soundTransform = new SoundTransform(this.volumeVal);
            }
            Tweener.addTween(this.controls.volume_bar.sp, {time:0.3, scaleX:this.volumeVal});
            this.muted = false;
            return;
        }// end function

        private function onVolBtnClick(event:MouseEvent) : void
        {
            if (!this.muted)
            {
                this.controls.vol_icon.gotoAndStop(2);
                if (this.songCh)
                {
                    this.songCh.soundTransform = new SoundTransform(0);
                }
                Tweener.addTween(this.controls.volume_bar.sp, {time:0.3, scaleX:0});
                this.muted = true;
            }
            else
            {
                this.controls.vol_icon.gotoAndStop(1);
                if (this.songCh)
                {
                    this.songCh.soundTransform = new SoundTransform(this.volumeVal);
                }
                Tweener.addTween(this.controls.volume_bar.sp, {time:0.3, scaleX:this.volumeVal});
                this.muted = false;
            }
            return;
        }// end function

        private function formatTime(param1:Number) : String
        {
            var _loc_2:* = param1 / 1000;
            var _loc_3:* = _loc_2 / 60;
            var _loc_4:* = _loc_2 - _loc_3 * 60;
            return String(_loc_3 < 10 ? ("0" + _loc_3) : (_loc_3)) + ":" + String(_loc_4 < 10 ? ("0" + _loc_4) : (_loc_4));
        }// end function

        public function duration() : Number
        {
            return this.song.length * this.song.bytesTotal / this.song.bytesLoaded;
        }// end function

        public function currentTime() : Number
        {
            return !this.paused && this.songCh ? (this.songCh.position) : (this.songCh.position);
        }// end function

        private function updateTitleAndLink() : void
        {
            this.album_tf.text = _xml.Album[currentAlbum].@title;
            this.RevealText(this.songList[_currentSong].@title, this.title_tf);
            if (this.songList[_currentSong].@link != "")
            {
                this.link_btn.visible = true;
                this.link_btn.addEventListener(MouseEvent.CLICK, this.onLinkClick);
            }
            else
            {
                this.link_btn.visible = false;
                this.link_btn.removeEventListener(MouseEvent.CLICK, this.onLinkClick);
            }
            return;
        }// end function

        public function _stop() : void
        {
            if (this.songCh)
            {
                this.songCh.stop();
            }
            return;
        }// end function

        private function onLinkClick(event:MouseEvent) : void
        {
            navigateToURL(new URLRequest(this.songList[_currentSong].@link), "_blank");
            return;
        }// end function

        private function colorThings() : void
        {
            Tweener.addTween(this.controls.progress_bar.sp, {_color:_xml.Settings.@progressBarColor});
            Tweener.addTween(this.controls.volume_bar.sp, {_color:_xml.Settings.@volumeBarColor});
            Tweener.addTween(this.controls.play_btn, {_color:_xml.Settings.@controlBtnColor});
            Tweener.addTween(this.controls.prev_btn, {_color:_xml.Settings.@controlBtnColor});
            Tweener.addTween(this.controls.next_btn, {_color:_xml.Settings.@controlBtnColor});
            Tweener.addTween(this.controls.vol_icon, {_color:_xml.Settings.@volumeIconColor});
            Tweener.addTween(this.title_tf, {_color:_xml.Settings.@textColor});
            Tweener.addTween(this.album_tf, {_color:_xml.Settings.@textColor});
            Tweener.addTween(this.controls.time_tf, {_color:_xml.Settings.@textColor});
            return;
        }// end function

        private function onBtnOver(event:MouseEvent) : void
        {
            Tweener.addTween(event.currentTarget, {_color:_xml.Settings.@controlBtnColorOver});
            return;
        }// end function

        private function onBtnOut(event:MouseEvent) : void
        {
            Tweener.addTween(event.currentTarget, {_color:_xml.Settings.@controlBtnColor});
            return;
        }// end function

        public function RevealText(param1:String, param2:TextField)
        {
            this.txt = param1;
            this.tf = param2;
            this.tf.text = "";
            this.newTxt = "";
            this.i = 0;
            this.timer = new Timer(10);
            this.timer.addEventListener(TimerEvent.TIMER, this.anim);
            this.timer.start();
            return;
        }// end function

        private function anim(event:TimerEvent) : void
        {
            this.newTxt = this.newTxt + this.txt.charAt(this.i);
            var _loc_2:String = this;
            var _loc_3:* = this.i + 1;
            _loc_2.i = _loc_3;
            this.tf.text = this.newTxt;
            if (this.i >= this.txt.length)
            {
                this.timer.reset();
                this.timer.removeEventListener(TimerEvent.TIMER, this.anim);
            }
            return;
        }// end function

    }
}
