package com.Ub.Music.playlist
{
    import caurina.transitions.*;
    import caurina.transitions.properties.*;
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;

    public class Track extends MovieClip
    {
        public var tf:TextField;
        private var title:String = "";
        private var id:int;
        private var colorOut:uint = 16777215;
        private var colorOver:uint = 16750950;
        private var newTxt:String = "";
        private var i:Number = 0;
        private var timer:Timer;
        private var delayTimer:Timer;
        private var selected:Boolean;

        public function Track(param1:String, param2:int)
        {
            ColorShortcuts.init();
            this.title = param1;
            this.id = param2;
            this.tf.text = "";
            addEventListener(Event.ADDED_TO_STAGE, this.init);
            return;
        }// end function

        private function init(event:Event) : void
        {
            Tweener.addTween(this, {_color:this.colorOut, time:0});
            var _loc_2:Boolean = true;
            this.useHandCursor = true;
            this.buttonMode = _loc_2;
            this.mouseChildren = false;
            addEventListener(MouseEvent.MOUSE_OVER, this.onMouseOver);
            addEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
            this.delayTimer = new Timer(this.id * 100, 0);
            this.delayTimer.addEventListener(TimerEvent.TIMER, this.delayStart);
            this.delayTimer.start();
            this.timer = new Timer(20, this.title.length);
            this.timer.addEventListener(TimerEvent.TIMER, this.anim);
            return;
        }// end function

        private function onMouseOver(event:MouseEvent) : void
        {
            this._colorTrack();
            return;
        }// end function

        private function onMouseOut(event:MouseEvent) : void
        {
            if (!this.selected)
            {
                this._discolorTrack();
            }
            return;
        }// end function

        private function delayStart(event:TimerEvent) : void
        {
            this.timer.start();
            this.delayTimer.removeEventListener(TimerEvent.TIMER, this.delayStart);
            return;
        }// end function

        private function anim(event:TimerEvent) : void
        {
            this.newTxt = this.newTxt + this.title.charAt(this.i);
            var _loc_2:String = this;
            var _loc_3:* = this.i + 1;
            _loc_2.i = _loc_3;
            this.tf.text = this.newTxt;
            return;
        }// end function

        public function _discolorTrack() : void
        {
            Tweener.addTween(this, {_color:this.colorOut, time:0.3});
            return;
        }// end function

        public function _colorTrack() : void
        {
            Tweener.addTween(this, {_color:this.colorOver, time:0.3});
            return;
        }// end function

        public function get _id() : int
        {
            return this.id;
        }// end function

        public function set _selected(param1:Boolean) : void
        {
            this.selected = param1;
            return;
        }// end function

        public function get _selected() : Boolean
        {
            return this.selected;
        }// end function

    }
}
