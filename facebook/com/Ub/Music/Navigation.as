package com.Ub.Music
{
    import caurina.transitions.*;
    import flash.display.*;
    import flash.events.*;

    public class Navigation extends Sprite
    {
        private var coverflow_icon:Sprite;
        private var playlist_icon:Sprite;
        private var play_icon:Sprite;
        private var visual_icon:Sprite;
        private var navArray:Array;
        private var _status:String;
        public static const COVERFLOW_ON:String = "COVERFLOW_ON";
        public static const PLAYLIST_ON:String = "PLAYLIST_ON";
        public static const PLAY_ON:String = "PLAY_ON";
        public static const VISUAL_ON:String = "VISUAL_ON";
        public static const CLICK:String = "menu_clicked";

        public function Navigation()
        {
            this.addBg();
            this.addIcons();
            this.status = PLAY_ON;
            this.updatePosition();
            return;
        }// end function

        private function addIcons() : void
        {
            var _loc_1:Sprite = null;
            this.coverflow_icon = new coverflow_btn();
            this.playlist_icon = new playlist_btn();
            this.play_icon = new play_btn();
            this.visual_icon = new visualizator_btn();
            this.navArray = [this.coverflow_icon, this.playlist_icon, this.play_icon, this.visual_icon];
            for each (_loc_1 in this.navArray)
            {
                
                var _loc_4:Boolean = true;
                _loc_1.useHandCursor = true;
                _loc_1.buttonMode = _loc_4;
                this._addEL();
                this.addChild(_loc_1);
            }
            return;
        }// end function

        private function navClicked(event:MouseEvent) : void
        {
            switch(event.target)
            {
                case this.coverflow_icon:
                {
                    this.status = COVERFLOW_ON;
                    break;
                }
                case this.playlist_icon:
                {
                    this.status = PLAYLIST_ON;
                    break;
                }
                case this.play_icon:
                {
                    this.status = PLAY_ON;
                    break;
                }
                case this.visual_icon:
                {
                    this.status = VISUAL_ON;
                    break;
                }
                default:
                {
                    break;
                }
            }
            this.updatePosition();
            dispatchEvent(new Event(CLICK));
            return;
        }// end function

        public function updatePosition() : void
        {
            switch(this._status)
            {
                case PLAY_ON:
                {
                    Tweener.addTween(this.play_icon, {y:30, time:0.5, alpha:0});
                    Tweener.addTween(this.playlist_icon, {x:7, y:6, time:0.5, alpha:1});
                    Tweener.addTween(this.coverflow_icon, {x:30, y:4, time:0.5, alpha:1});
                    Tweener.addTween(this.visual_icon, {x:53, y:3, time:0.5, alpha:1});
                    break;
                }
                case PLAYLIST_ON:
                {
                    Tweener.addTween(this.playlist_icon, {y:30, time:0.5, alpha:0});
                    Tweener.addTween(this.play_icon, {x:10, y:4, time:0.5, alpha:1});
                    Tweener.addTween(this.coverflow_icon, {x:30, y:4, time:0.5, alpha:1});
                    Tweener.addTween(this.visual_icon, {x:53, y:3, time:0.5, alpha:1});
                    break;
                }
                case COVERFLOW_ON:
                {
                    Tweener.addTween(this.coverflow_icon, {y:30, time:0.5, alpha:0});
                    Tweener.addTween(this.play_icon, {x:10, y:4, time:0.5, alpha:1});
                    Tweener.addTween(this.playlist_icon, {x:28, y:6, time:0.5, alpha:1});
                    Tweener.addTween(this.visual_icon, {x:53, y:3, time:0.5, alpha:1});
                    break;
                }
                case VISUAL_ON:
                {
                    Tweener.addTween(this.visual_icon, {y:30, time:0.5, alpha:0});
                    Tweener.addTween(this.play_icon, {x:10, y:4, time:0.5, alpha:1});
                    Tweener.addTween(this.playlist_icon, {x:28, y:6, time:0.5, alpha:1});
                    Tweener.addTween(this.coverflow_icon, {x:49, y:4, time:0.5, alpha:1});
                    break;
                }
                default:
                {
                    break;
                }
            }
            return;
        }// end function

        private function addBg() : void
        {
            var _loc_1:* = new Sprite();
            _loc_1.graphics.beginFill(0, 0.2);
            _loc_1.graphics.drawRoundRect(0, 0, 70, 22, 5, 5);
            _loc_1.graphics.endFill();
            this.addChild(_loc_1);
            return;
        }// end function

        public function _addEL() : void
        {
            var _loc_1:Sprite = null;
            for each (_loc_1 in this.navArray)
            {
                
                _loc_1.addEventListener(MouseEvent.CLICK, this.navClicked);
            }
            return;
        }// end function

        public function _removeEL() : void
        {
            var _loc_1:Sprite = null;
            for each (_loc_1 in this.navArray)
            {
                
                _loc_1.removeEventListener(MouseEvent.CLICK, this.navClicked);
            }
            return;
        }// end function

        public function get status() : String
        {
            return this._status;
        }// end function

        public function set status(param1:String) : void
        {
            this._status = param1;
            return;
        }// end function

    }
}
