package com.Ub.Music
{
    import br.com.stimuli.loading.*;
    import caurina.transitions.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.net.*;
    import flash.utils.*;

    public class Coverflow extends MusicCore
    {
        protected var coverArray:Array;
        private var currentItem:Number = 0;
        private var nextItem:int;
        private var distanceX:Number = 280;
        private var angle:Number = 10;
        private var container:Sprite;
        private var scaleDiv:Number = 1.5;
        private var opened:Boolean;
        private var sortTimer:Timer;
        private var assetsLoader:BulkLoader;
        public static const NEW_ALBUM_CLICKED:String = "new_clicked";
        public static const CURRENT_ALBUM_CLICKED:String = "old_clicked";

        public function Coverflow() : void
        {
            this.container = new Sprite();
            return;
        }// end function

        override protected function init(event:Event) : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, this.init);
            this._setCovers();
            this.container.x = stage.stageWidth / 2;
            this.container.y = 170;
            this.loadAssets();
            var _loc_2:* = root.transform.perspectiveProjection;
            _loc_2.projectionCenter = new Point(stage.stageWidth / 2, 170);
            stage.transform.perspectiveProjection = _loc_2;
            return;
        }// end function

        override public function COVERFLOW_ON() : void
        {
            var _loc_2:Cover = null;
            this._reveal();
            Tweener.addTween(this, {delay:0.5, scaleX:1, scaleY:1, time:1, x:0, y:0});
            this.toggleCoverflow(true);
            var _loc_1:int = 0;
            while (_loc_1 < this.coverArray.length)
            {
                
                _loc_2 = this.coverArray[_loc_1];
                _loc_2.addEventListener(MouseEvent.CLICK, this.onCoverClick);
                _loc_2.addEventListener(MouseEvent.MOUSE_OVER, this.onCoverOver);
                _loc_2.addEventListener(MouseEvent.MOUSE_OUT, this.onCoverOut);
                _loc_1++;
            }
            return;
        }// end function

        override public function PLAY_ON() : void
        {
            var _loc_2:Cover = null;
            this._reveal();
            this.currentItem = _currentAlbum;
            Tweener.addTween(this, {delay:0.5, scaleX:1, scaleY:1, time:1, x:-110, y:0});
            this.toggleCoverflow(false);
            var _loc_1:int = 0;
            while (_loc_1 < this.coverArray.length)
            {
                
                _loc_2 = this.coverArray[_loc_1];
                _loc_2.removeEventListener(MouseEvent.CLICK, this.onCoverClick);
                _loc_2.removeEventListener(MouseEvent.MOUSE_OVER, this.onCoverOver);
                _loc_2.removeEventListener(MouseEvent.MOUSE_OUT, this.onCoverOut);
                _loc_2.load_album_btn.visible = false;
                _loc_1++;
            }
            return;
        }// end function

        override public function PLAYLIST_ON() : void
        {
            var _loc_2:Cover = null;
            this._reveal();
            this.currentItem = _currentAlbum;
            Tweener.addTween(this, {delay:0.5, scaleX:0.8, scaleY:0.8, time:1, x:-80, y:30});
            this.toggleCoverflow(false);
            var _loc_1:int = 0;
            while (_loc_1 < this.coverArray.length)
            {
                
                _loc_2 = this.coverArray[_loc_1];
                _loc_2.removeEventListener(MouseEvent.CLICK, this.onCoverClick);
                _loc_2.removeEventListener(MouseEvent.MOUSE_OVER, this.onCoverOver);
                _loc_2.removeEventListener(MouseEvent.MOUSE_OUT, this.onCoverOut);
                _loc_2.load_album_btn.visible = false;
                _loc_1++;
            }
            return;
        }// end function

        override public function VISUALIZER_ON() : void
        {
            this._hide();
            Tweener.addTween(this, {delay:0.5, scaleX:1, scaleY:1, time:1, x:-110, y:0});
            this.toggleCoverflow(false);
            return;
        }// end function

        private function loadAssets() : void
        {
            var _loc_2:URLRequest = null;
            this.assetsLoader = new BulkLoader();
            this.assetsLoader.addEventListener(BulkProgressEvent.COMPLETE, this.onAssetsLoaded);
            var _loc_1:int = 0;
            while (_loc_1 < _xml.Album.length())
            {
                
                _loc_2 = new URLRequest(_xml.Album[_loc_1].@cover);
                this.assetsLoader.add(_loc_2, {id:"cover" + _loc_1});
                _loc_1++;
            }
            this.assetsLoader.start();
            return;
        }// end function

        private function onAssetsLoaded(event:BulkProgressEvent) : void
        {
            var _loc_4:Bitmap = null;
            var _loc_2:* = new Array();
            var _loc_3:int = 0;
            while (_loc_3 < this.assetsLoader.items.length)
            {
                
                _loc_4 = this.assetsLoader.get("cover" + _loc_3).content as Bitmap;
                _loc_4.alpha = 0;
                this.coverArray[_loc_3].image.addChild(_loc_4);
                Tweener.addTween(_loc_4, {time:0.5, alpha:1});
                _loc_3++;
            }
            return;
        }// end function

        public function _setCovers()
        {
            var _loc_2:Cover = null;
            this.addChild(this.container);
            this.coverArray = new Array();
            var _loc_1:int = 0;
            while (_loc_1 < _xml.Album.length())
            {
                
                _loc_2 = new Cover();
                _loc_2.id = _loc_1;
                _loc_2.load_album_btn.alpha = 0;
                this.container.addChild(_loc_2);
                this.coverArray.push(_loc_2);
                Tweener.addTween(_loc_2, {time:0.5, alpha:1});
                _loc_2.addEventListener(MouseEvent.CLICK, this.onCoverClick);
                _loc_2.addEventListener(MouseEvent.MOUSE_OVER, this.onCoverOver);
                _loc_2.addEventListener(MouseEvent.MOUSE_OUT, this.onCoverOut);
                _loc_1++;
            }
            return;
        }// end function

        private function onCoverClick(event:MouseEvent) : void
        {
            this.nextItem = (event.currentTarget as Cover).id;
            if (this.opened)
            {
                if (this.currentItem == this.nextItem && this.currentItem != _currentAlbum)
                {
                    _currentAlbum = this.currentItem;
                    dispatchEvent(new Event(NEW_ALBUM_CLICKED));
                }
                else if (this.currentItem == this.nextItem && this.currentItem == _currentAlbum)
                {
                    dispatchEvent(new Event(CURRENT_ALBUM_CLICKED));
                }
                else if (this.currentItem != this.nextItem)
                {
                    this.currentItem = this.nextItem;
                    this.toggleCoverflow(true);
                }
            }
            else
            {
                this.currentItem = _currentAlbum;
                this.toggleCoverflow(true);
            }
            return;
        }// end function

        private function toggleCoverflow(param1:Boolean) : void
        {
            var _loc_3:Cover = null;
            var _loc_4:Number = NaN;
            var _loc_5:Number = NaN;
            var _loc_6:Number = NaN;
            var _loc_7:Number = NaN;
            this.opened = param1;
            var _loc_2:int = 0;
            while (_loc_2 < this.coverArray.length)
            {
                
                _loc_3 = this.coverArray[_loc_2] as Cover;
                if (this.opened)
                {
                    _loc_4 = 1;
                }
                else
                {
                    _loc_4 = 0;
                }
                _loc_3._selected = false;
                _loc_5 = 0;
                _loc_6 = 0;
                _loc_7 = 1;
                if (_loc_2 == this.currentItem)
                {
                    _loc_3._selected = param1;
                    Tweener.addTween(_loc_3, {time:1, alpha:1, rotationY:_loc_5, scaleX:_loc_7, scaleY:_loc_7, x:_loc_6, transition:"easeInOutQuint"});
                }
                if (_loc_2 > this.currentItem)
                {
                    if (this.opened)
                    {
                        _loc_6 = this.distanceX * (_loc_2 - this.currentItem) / (_loc_2 - this.currentItem + 1);
                    }
                    else
                    {
                        _loc_6 = 0;
                    }
                    _loc_7 = this.scaleDiv / ((_loc_2 + 1) - this.currentItem);
                    _loc_5 = (-this.angle) * ((_loc_2 + 1) - this.currentItem);
                    if (_loc_5 < -55)
                    {
                        _loc_5 = -55;
                    }
                    Tweener.addTween(_loc_3, {time:1, alpha:_loc_4, rotationY:_loc_5, scaleX:_loc_7, scaleY:_loc_7, x:_loc_6, transition:"easeInOutQuint"});
                }
                if (_loc_2 < this.currentItem)
                {
                    if (this.opened)
                    {
                        _loc_6 = (-this.distanceX) * (this.currentItem - _loc_2) / (this.currentItem - _loc_2 + 1);
                    }
                    else
                    {
                        _loc_6 = 0;
                    }
                    _loc_7 = this.scaleDiv / (this.currentItem - _loc_2 + 1);
                    _loc_5 = this.angle * (this.currentItem - _loc_2 + 1);
                    if (_loc_5 > 55)
                    {
                        _loc_5 = 55;
                    }
                    Tweener.addTween(_loc_3, {time:1, alpha:_loc_4, rotationY:_loc_5, scaleX:_loc_7, scaleY:_loc_7, x:_loc_6, transition:"easeInOutQuint"});
                }
                _loc_2++;
            }
            this.sortTimer = new Timer(500, 1);
            this.sortTimer.addEventListener(TimerEvent.TIMER, this.zSort);
            this.sortTimer.start();
            return;
        }// end function

        private function zSort(event:TimerEvent) : void
        {
            var _loc_2:int = 0;
            while (_loc_2 < this.currentItem)
            {
                
                this.container.addChild(this.coverArray[_loc_2]);
                _loc_2++;
            }
            var _loc_3:* = this.coverArray.length - 1;
            while (_loc_3 > this.currentItem)
            {
                
                this.container.addChild(this.coverArray[_loc_3]);
                _loc_3 = _loc_3 - 1;
            }
            this.container.addChild(this.coverArray[this.currentItem]);
            this.sortTimer.removeEventListener(TimerEvent.TIMER, this.zSort);
            this.sortTimer.reset();
            return;
        }// end function

        private function onCoverOver(event:MouseEvent) : void
        {
            var _loc_2:* = event.currentTarget as Cover;
            if (_loc_2._selected)
            {
                _loc_2.load_album_btn.alpha = 0;
                _loc_2.load_album_btn.visible = true;
                Tweener.addTween(_loc_2.load_album_btn, {time:1, alpha:1});
            }
            return;
        }// end function

        private function onCoverOut(event:MouseEvent) : void
        {
            var tempCover:Cover;
            var e:* = event;
            tempCover = e.currentTarget as Cover;
            if (tempCover._selected)
            {
                Tweener.addTween(tempCover.load_album_btn, {time:1, alpha:0, onComplete:function () : void
            {
                tempCover.load_album_btn.visible = false;
                return;
            }// end function
            });
            }
            return;
        }// end function

    }
}
