package com.Ub.Music
{
    import br.com.stimuli.loading.*;
    import com.Ub.*;
    import flash.display.*;
    import flash.events.*;

    public class MusicModule extends Module
    {
        public var _container:MovieClip;
        private var nav:Navigation;
        private var play_mc:Play;
        private var playlist_mc:Playlist;
        private var visualizer_mc:Visualizer;
        private var coverflow_mc:Coverflow;
        private var mcArray:Array;
        private var currAlbum:int;
        private var currSong:int;
        private var assetsLoader:BulkLoader;

        public function MusicModule() : void
        {
            return;
        }// end function

        override protected function init(event:Event) : void
        {
            var _loc_2:MusicCore = null;
            if (_xml)
            {
                this.addNavigation();
                this.play_mc = new Play();
                this.coverflow_mc = new Coverflow();
                this.playlist_mc = new Playlist();
                this.visualizer_mc = new Visualizer();
                this.mcArray = [this.play_mc, this.coverflow_mc, this.playlist_mc, this.visualizer_mc];
                this.coverflow_mc.addEventListener(Coverflow.NEW_ALBUM_CLICKED, this.onNewCoverClicked);
                this.coverflow_mc.addEventListener(Coverflow.CURRENT_ALBUM_CLICKED, this.onCurrentCoverClicked);
                this.playlist_mc.addEventListener(Playlist.TRACK_CLICKED, this.onTrackClicked);
                this.play_mc.addEventListener(Play.SONG_CHANGED, this.onSongChanged);
                for each (_loc_2 in this.mcArray)
                {
                    
                    _loc_2.xml = _xml;
                    _loc_2.addEventListener("hide", this.removeElNav);
                    _loc_2.addEventListener("reveal", this.addElNav);
                }
                this.openFirst();
            }
            return;
        }// end function

        private function addElNav(event:Event) : void
        {
            this.nav._addEL();
            return;
        }// end function

        private function removeElNav(event:Event) : void
        {
            this.nav._removeEL();
            return;
        }// end function

        private function openFirst() : void
        {
            var _loc_1:MusicCore = null;
            for each (_loc_1 in this.mcArray)
            {
                
                _loc_1.xml = _xml;
                _content.addChild(_loc_1);
                _loc_1.alpha = 0;
                if (_xml.Settings.@autoPlay == "true")
                {
                    _loc_1.currentAlbum = 0;
                    _loc_1.PLAY_ON();
                    this.nav.visible = true;
                    continue;
                }
                _loc_1.COVERFLOW_ON();
            }
            if (_xml.Settings.@autoPlay == "true")
            {
                this.play_mc._playNewAlbum();
                this.nav.status = Navigation.PLAY_ON;
            }
            else
            {
                this.nav.status = Navigation.COVERFLOW_ON;
            }
            return;
        }// end function

        private function addNavigation() : void
        {
            this.nav = new Navigation();
            this.nav.x = stage.stageWidth / 2 - 35;
            this.nav.y = 260;
            this.nav.addEventListener(Navigation.CLICK, this.onNavClick);
            this.nav.visible = false;
            _content.addChild(this.nav);
            return;
        }// end function

        private function onNavClick(event:Event) : void
        {
            var _loc_2:MusicCore = null;
            for each (_loc_2 in this.mcArray)
            {
                
                _loc_2.changeStatus(this.nav.status);
            }
            return;
        }// end function

        private function onNewCoverClicked(event:Event) : void
        {
            var _loc_2:MusicCore = null;
            for each (_loc_2 in this.mcArray)
            {
                
                _loc_2.currentAlbum = this.coverflow_mc.currentAlbum;
                _loc_2.currentSong = 0;
                _loc_2.PLAY_ON();
            }
            this.nav.status = Navigation.PLAY_ON;
            this.nav.updatePosition();
            this.play_mc._playNewAlbum();
            if (!this.nav.visible)
            {
                this.nav.visible = true;
            }
            return;
        }// end function

        private function onCurrentCoverClicked(event:Event) : void
        {
            var _loc_2:MusicCore = null;
            for each (_loc_2 in this.mcArray)
            {
                
                _loc_2.PLAY_ON();
            }
            this.nav.status = Navigation.PLAY_ON;
            this.nav.updatePosition();
            return;
        }// end function

        private function onTrackClicked(event:Event) : void
        {
            this.play_mc.playSong(this.playlist_mc.currentSong);
            return;
        }// end function

        private function onSongChanged(event:Event) : void
        {
            this.playlist_mc.currentSong = this.play_mc.currentSong;
            return;
        }// end function

        override public function _hide() : void
        {
            this.play_mc._stop();
            return;
        }// end function

        private function onLoaderError(event:Event) : void
        {
            return;
        }// end function

        public function get _currAlbum() : int
        {
            return this.currAlbum;
        }// end function

        public function set _currAlbum(param1:int) : void
        {
            this.currAlbum = param1;
            return;
        }// end function

        public function get _currSong() : int
        {
            return this.currSong;
        }// end function

        public function set _currSong(param1:int) : void
        {
            this.currSong = param1;
            return;
        }// end function

    }
}
