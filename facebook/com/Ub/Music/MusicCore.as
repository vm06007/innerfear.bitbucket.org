package com.Ub.Music
{
    import caurina.transitions.*;
    import flash.display.*;
    import flash.events.*;

    public class MusicCore extends MovieClip
    {
        protected var _xml:XML;
        protected var _currentAlbum:Number = -1;
        protected var _currentSong:Number = -1;
        private var _inTransition:Boolean;

        public function MusicCore()
        {
            addEventListener(Event.ADDED_TO_STAGE, this.init);
            return;
        }// end function

        protected function init(event:Event) : void
        {
            this.alpha = 0;
            return;
        }// end function

        public function PLAY_ON() : void
        {
            this._hide();
            return;
        }// end function

        public function PLAYLIST_ON() : void
        {
            this._hide();
            return;
        }// end function

        public function COVERFLOW_ON() : void
        {
            this._hide();
            return;
        }// end function

        public function VISUALIZER_ON() : void
        {
            this._hide();
            return;
        }// end function

        protected function _hide() : void
        {
            this._inTransition = true;
            dispatchEvent(new Event("hide"));
            Tweener.addTween(this, {time:0.5, alpha:0, onComplete:function () : void
            {
                visible = false;
                return;
            }// end function
            });
            return;
        }// end function

        protected function _reveal() : void
        {
            visible = true;
            Tweener.addTween(this, {delay:0.5, time:1, alpha:1, onComplete:function () : void
            {
                _inTransition = false;
                dispatchEvent(new Event("reveal"));
                return;
            }// end function
            });
            return;
        }// end function

        public function changeStatus(param1:String) : void
        {
            switch(param1)
            {
                case "PLAY_ON":
                {
                    this.PLAY_ON();
                    break;
                }
                case "PLAYLIST_ON":
                {
                    this.PLAYLIST_ON();
                    break;
                }
                case "VISUAL_ON":
                {
                    this.VISUALIZER_ON();
                    break;
                }
                case "COVERFLOW_ON":
                {
                    this.COVERFLOW_ON();
                    break;
                }
                default:
                {
                    break;
                }
            }
            return;
        }// end function

        public function get xml() : XML
        {
            return this._xml;
        }// end function

        public function set xml(param1:XML) : void
        {
            this._xml = param1;
            return;
        }// end function

        public function get currentAlbum() : Number
        {
            return this._currentAlbum;
        }// end function

        public function set currentAlbum(param1:Number) : void
        {
            this._currentAlbum = param1;
            return;
        }// end function

        public function get currentSong() : Number
        {
            return this._currentSong;
        }// end function

        public function set currentSong(param1:Number) : void
        {
            this._currentSong = param1;
            return;
        }// end function

    }
}
