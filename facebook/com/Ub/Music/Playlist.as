package com.Ub.Music
{
    import caurina.transitions.*;
    import caurina.transitions.properties.*;
    import com.Ub.Music.playlist.*;
    import com.Ub.utils.*;
    import flash.display.*;
    import flash.events.*;

    public class Playlist extends MusicCore
    {
        private var songArray:Array;
        private var songList:XMLList;
        private var trackContainer:Sprite;
        private var contMask:Sprite;
        private var scrollbar:ScrollBar;
        private var colorOut:uint = 16777215;
        private var colorOver:uint = 16750950;
        public static const TRACK_CLICKED:String = "trackClicked";

        public function Playlist()
        {
            return;
        }// end function

        override protected function init(event:Event) : void
        {
            ColorShortcuts.init();
            removeEventListener(Event.ADDED_TO_STAGE, this.init);
            this.trackContainer = new Sprite();
            this.addChild(this.trackContainer);
            this.makeMaskAndScroll();
            return;
        }// end function

        override public function PLAYLIST_ON() : void
        {
            this._reveal();
            this.x = 200;
            this.y = 110;
            this.updatePlaylist();
            return;
        }// end function

        override protected function _reveal() : void
        {
            this.alpha = 0;
            visible = true;
            Tweener.addTween(this, {delay:1, time:1, alpha:1});
            return;
        }// end function

        private function updatePlaylist()
        {
            var _loc_3:* = undefined;
            this.clearPlaylist();
            this.songList = _xml.Album[_currentAlbum].song;
            this.songArray = new Array();
            var _loc_1:Number = 0;
            var _loc_2:int = 0;
            while (_loc_2 < this.songList.length())
            {
                
                _loc_3 = new Track(this.songList[_loc_2].@title, _loc_2);
                _loc_3.y = _loc_3.height * _loc_2;
                this.trackContainer.addChild(_loc_3);
                _loc_1 = _loc_1 + _loc_3.height;
                _loc_3._selected = false;
                _loc_3._discolorTrack();
                _loc_3.addEventListener(MouseEvent.CLICK, this.onTrackClick);
                this.songArray.push(_loc_3);
                _loc_2++;
            }
            this.trackContainer.height = _loc_1;
            if (this.scrollbar)
            {
                this.scrollbar.init();
            }
            Tweener.addTween(this.songArray[_currentSong], {_color:this.colorOver, time:0.3});
            (this.songArray[_currentSong] as Track)._selected = true;
            return;
        }// end function

        private function onTrackClick(event:MouseEvent) : void
        {
            Tweener.addTween(this.songArray[currentSong], {_color:this.colorOut, time:0.3});
            (this.songArray[_currentSong] as Track)._selected = false;
            _currentSong = (event.currentTarget as Track)._id;
            this.dispatchEvent(new Event(TRACK_CLICKED));
            Tweener.addTween(this.songArray[currentSong], {_color:this.colorOver, time:0.3});
            (this.songArray[_currentSong] as Track)._selected = true;
            return;
        }// end function

        private function makeMaskAndScroll() : void
        {
            this.contMask = new Sprite();
            this.contMask.graphics.beginFill(65280);
            this.contMask.graphics.drawRect(0, 0, 220, 130);
            this.contMask.graphics.endFill();
            this.addChild(this.contMask);
            this.trackContainer.mask = this.contMask;
            this.scrollbar = new ScrollBar();
            this.scrollbar.contentMask = this.contMask;
            this.scrollbar.content = this.trackContainer;
            this.scrollbar.x = -10;
            this.scrollbar.y = 5;
            this.addChild(this.scrollbar);
            this.scrollbar.init();
            return;
        }// end function

        private function clearPlaylist() : void
        {
            var _loc_1:* = this.trackContainer.numChildren - 1;
            while (_loc_1 >= 0)
            {
                
                this.trackContainer.removeChildAt(_loc_1);
                _loc_1 = _loc_1 - 1;
            }
            return;
        }// end function

    }
}
