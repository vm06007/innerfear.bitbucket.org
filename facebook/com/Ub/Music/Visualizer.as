package com.Ub.Music
{
    import fl.motion.easing.*;
    import flash.display.*;
    import flash.events.*;
    import flash.media.*;
    import flash.utils.*;

    public class Visualizer extends MusicCore
    {
        private var bar_height:int = 100;
        private var bar_width:int = 1;
        private var bar_distance:int = 2;
        private var num_of_bars:int = 128;
        private var barArray:Array;
        private var barContainer:Sprite;
        private const VALUES_PER_SEGMENT:int;
        private var segmentTotals:Array;

        public function Visualizer()
        {
            this.VALUES_PER_SEGMENT = Math.floor(256 / this.num_of_bars);
            return;
        }// end function

        override protected function init(event:Event) : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, this.init);
            addEventListener(Event.REMOVED_FROM_STAGE, this.onRemoved);
            this.createBar(16777215, this.num_of_bars);
            addEventListener(Event.ENTER_FRAME, this.onEnterFrame);
            this.segmentTotals = new Array(this.num_of_bars);
            return;
        }// end function

        override public function VISUALIZER_ON() : void
        {
            this._reveal();
            return;
        }// end function

        function createBar(param1:Number, param2:Number) : void
        {
            var _loc_4:Sprite = null;
            this.barContainer = new Sprite();
            this.barArray = new Array();
            var _loc_3:int = 0;
            while (_loc_3 < param2)
            {
                
                _loc_4 = new Sprite();
                _loc_4.graphics.beginFill(param1, 1);
                _loc_4.graphics.drawRect(0, -1, this.bar_width, 2);
                _loc_4.graphics.endFill();
                this.barArray.push(_loc_4);
                _loc_4.x = _loc_3 * (this.bar_width + this.bar_distance);
                this.barContainer.addChild(_loc_4);
                _loc_3++;
            }
            this.addChild(this.barContainer);
            this.barContainer.x = int((stage.stageWidth - this.barContainer.width) / 2);
            this.barContainer.y = int(stage.stageHeight / 2);
            return;
        }// end function

        private function onEnterFrame(event:Event) : void
        {
            var _loc_7:int = 0;
            var _loc_10:Sprite = null;
            var _loc_2:Number = 0;
            var _loc_3:Number = 0;
            var _loc_4:Number = 0;
            var _loc_5:Number = 0;
            var _loc_6:Number = 0;
            var _loc_8:Number = 0;
            _loc_7 = 0;
            while (_loc_7 < this.segmentTotals.length)
            {
                
                this.segmentTotals[_loc_7] = 0;
                _loc_7++;
            }
            var _loc_9:* = new ByteArray();
            SoundMixer.computeSpectrum(_loc_9, true, 0);
            _loc_3 = 0;
            while (_loc_3 < this.num_of_bars)
            {
                
                _loc_7 = 0;
                while (_loc_7 < this.VALUES_PER_SEGMENT)
                {
                    
                    _loc_8 = Math.abs(_loc_9.readFloat());
                    this.segmentTotals[_loc_3] = this.segmentTotals[_loc_3] + _loc_8;
                    _loc_4 = _loc_4 + 1;
                    _loc_7++;
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_6 = 256 - _loc_4;
            if (_loc_6 > 0)
            {
                _loc_7 = 0;
                while (_loc_7 < _loc_6)
                {
                    
                    _loc_9.readFloat();
                    _loc_4 = _loc_4 + 1;
                    _loc_7++;
                }
            }
            _loc_4 = 0;
            _loc_3 = 0;
            while (_loc_3 < this.num_of_bars)
            {
                
                _loc_7 = 0;
                while (_loc_7 < this.VALUES_PER_SEGMENT)
                {
                    
                    _loc_8 = Math.abs(_loc_9.readFloat());
                    this.segmentTotals[_loc_3] = this.segmentTotals[_loc_3] + _loc_8;
                    _loc_4 = _loc_4 + 1;
                    _loc_7++;
                }
                _loc_3 = _loc_3 + 1;
            }
            _loc_7 = 0;
            while (_loc_7 < this.segmentTotals.length)
            {
                
                _loc_5 = this.segmentTotals[_loc_7] / (this.VALUES_PER_SEGMENT * 2);
                _loc_5 = Quadratic.easeOut(_loc_5, 0, 1, 1);
                if (_loc_5 > 1)
                {
                    _loc_5 = 1;
                }
                _loc_2 = _loc_5 * this.bar_height;
                _loc_10 = this.barArray[_loc_7] as Sprite;
                this.scaleBarUp(_loc_10, _loc_2);
                _loc_7++;
            }
            return;
        }// end function

        function scaleBarUp(param1:Sprite, param2:Number)
        {
            param1.scaleY = param1.scaleY + (param2 - param1.height) * 0.3;
            return;
        }// end function

        private function onRemoved(event:Event) : void
        {
            removeEventListener(Event.REMOVED_FROM_STAGE, this.onRemoved);
            removeEventListener(Event.ENTER_FRAME, this.onEnterFrame);
            return;
        }// end function

    }
}
