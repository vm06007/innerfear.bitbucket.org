package com.Ub.utils
{
    import caurina.transitions.*;
    import caurina.transitions.properties.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;

    public class ScrollBar extends MovieClip
    {
        public var scrollThumb:MovieClip;
        public var guide:MovieClip;
        private var _content:DisplayObject;
        private var _contentMask:DisplayObject;
        private var _thumbColorOnOver:String = "0xffcc66";
        private var _thumbColorOnOut:String = "0xffffff";
        private var scrollThumbHold:Boolean = false;
        private var mouseOverScrollThumb:Boolean = false;

        public function ScrollBar()
        {
            return;
        }// end function

        public function init() : void
        {
            ColorShortcuts.init();
            this.scrollThumb.buttonMode = true;
            this.scrollThumb.useHandCursor = true;
            this.scrollThumb.mouseChildren = false;
            stage.addEventListener(MouseEvent.MOUSE_UP, this.onThumbUp);
            stage.addEventListener(MouseEvent.MOUSE_WHEEL, this.onMouseWheel);
            this.scrollThumb.addEventListener(MouseEvent.MOUSE_OVER, this.onMouseOver);
            this.scrollThumb.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
            this.height = this.guide.height;
            this.update();
            Tweener.addTween(this, {alpha:0, time:0});
            Tweener.addTween(this, {alpha:1, time:3});
            return;
        }// end function

        public function update() : void
        {
            if (this._content != null && this._contentMask != null)
            {
                if (Tweener.isTweening(this._content))
                {
                    Tweener.removeTweens(this._content);
                }
                this.scrollThumb.stopDrag();
                this.scrollThumb.y = 0;
                this._content.y = this._contentMask.y;
                if (this._content.height <= this._contentMask.height)
                {
                    visible = false;
                    stage.removeEventListener(MouseEvent.MOUSE_WHEEL, this.onMouseWheel);
                    stage.removeEventListener(MouseEvent.MOUSE_UP, this.onThumbUp);
                    this.scrollThumb.removeEventListener(MouseEvent.MOUSE_OVER, this.onMouseOver);
                    this.scrollThumb.removeEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
                }
                else
                {
                    visible = true;
                    this.scrollThumb.addEventListener(MouseEvent.MOUSE_DOWN, this.onThumbDown, false, 0, true);
                }
            }
            return;
        }// end function

        private function onThumbDown(event:MouseEvent) : void
        {
            this.scrollThumbHold = true;
            this.scrollThumb.startDrag(false, new Rectangle(0, 0, 0, this.guide.height - this.scrollThumb.height));
            addEventListener(Event.ENTER_FRAME, this.onEnterFrame, false, 0, true);
            return;
        }// end function

        private function onThumbUp(event:MouseEvent) : void
        {
            this.scrollThumbHold = false;
            this.scrollThumb.stopDrag();
            removeEventListener(Event.ENTER_FRAME, this.onEnterFrame);
            if (!this.mouseOverScrollThumb)
            {
                Tweener.addTween(this.scrollThumb, {_color:this._thumbColorOnOut, time:1});
            }
            return;
        }// end function

        private function onMouseOver(event:MouseEvent) : void
        {
            this.mouseOverScrollThumb = true;
            Tweener.addTween(this.scrollThumb, {_color:this._thumbColorOnOver, time:1});
            return;
        }// end function

        private function onMouseOut(event:MouseEvent) : void
        {
            this.mouseOverScrollThumb = false;
            if (!this.scrollThumbHold)
            {
                Tweener.addTween(this.scrollThumb, {_color:this._thumbColorOnOut, time:1});
            }
            return;
        }// end function

        private function onMouseWheel(event:MouseEvent) : void
        {
            var _loc_2:Number = NaN;
            if (event.delta < 0)
            {
                if (this.scrollThumb.y < this.guide.height - this.scrollThumb.height)
                {
                    _loc_2 = this.guide.height - this.scrollThumb.height - this.scrollThumb.y;
                    if (_loc_2 >= 10)
                    {
                        this.scrollThumb.y = this.scrollThumb.y + 10;
                    }
                    else
                    {
                        this.scrollThumb.y = this.scrollThumb.y + _loc_2;
                    }
                }
            }
            else if (this.scrollThumb.y > 0)
            {
                _loc_2 = this.scrollThumb.y;
                if (this.scrollThumb.y >= 10)
                {
                    this.scrollThumb.y = this.scrollThumb.y - 10;
                }
                else
                {
                    this.scrollThumb.y = 0;
                }
            }
            this.animate();
            return;
        }// end function

        private function onEnterFrame(event:Event) : void
        {
            this.animate();
            return;
        }// end function

        private function animate() : void
        {
            var _loc_1:* = this._contentMask.y + this.scrollThumb.y * (this._contentMask.height - this._content.height) / (this.guide.height - this.scrollThumb.height);
            Tweener.addTween(this._content, {y:_loc_1, time:0.5, transition:Equations.easeOutQuint});
            return;
        }// end function

        public function set content(param1:DisplayObject) : void
        {
            this._content = param1;
            return;
        }// end function

        public function set contentMask(param1:DisplayObject) : void
        {
            this._contentMask = param1;
            return;
        }// end function

        public function set thumbColorOnOver(param1:String) : void
        {
            this._thumbColorOnOver = param1;
            return;
        }// end function

        public function set thumbColorOnOut(param1:String) : void
        {
            this._thumbColorOnOut = param1;
            return;
        }// end function

    }
}
