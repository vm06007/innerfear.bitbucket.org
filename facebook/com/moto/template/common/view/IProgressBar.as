package com.moto.template.common.view
{

    public interface IProgressBar
    {

        public function IProgressBar();

        function get percentLoaded() : int;

        function get total() : Number;

        function get loaded() : Number;

        function setProgress(param1:Number, param2:Number) : void;

    }
}
