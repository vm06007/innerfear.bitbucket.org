package caurina.transitions.properties
{
    import caurina.transitions.*;
    import flash.filters.*;
    import flash.geom.*;

    public class ColorShortcuts extends Object
    {
        private static var LUMINANCE_R:Number = 0.212671;
        private static var LUMINANCE_G:Number = 0.71516;
        private static var LUMINANCE_B:Number = 0.072169;

        public function ColorShortcuts()
        {
            trace("This is an static class and should not be instantiated.");
            return;
        }// end function

        public static function init() : void
        {
            Tweener.registerSpecialProperty("_color_ra", _oldColor_property_get, _oldColor_property_set, ["redMultiplier"]);
            Tweener.registerSpecialProperty("_color_rb", _color_property_get, _color_property_set, ["redOffset"]);
            Tweener.registerSpecialProperty("_color_ga", _oldColor_property_get, _oldColor_property_set, ["greenMultiplier"]);
            Tweener.registerSpecialProperty("_color_gb", _color_property_get, _color_property_set, ["greenOffset"]);
            Tweener.registerSpecialProperty("_color_ba", _oldColor_property_get, _oldColor_property_set, ["blueMultiplier"]);
            Tweener.registerSpecialProperty("_color_bb", _color_property_get, _color_property_set, ["blueOffset"]);
            Tweener.registerSpecialProperty("_color_aa", _oldColor_property_get, _oldColor_property_set, ["alphaMultiplier"]);
            Tweener.registerSpecialProperty("_color_ab", _color_property_get, _color_property_set, ["alphaOffset"]);
            Tweener.registerSpecialProperty("_color_redMultiplier", _color_property_get, _color_property_set, ["redMultiplier"]);
            Tweener.registerSpecialProperty("_color_redOffset", _color_property_get, _color_property_set, ["redOffset"]);
            Tweener.registerSpecialProperty("_color_greenMultiplier", _color_property_get, _color_property_set, ["greenMultiplier"]);
            Tweener.registerSpecialProperty("_color_greenOffset", _color_property_get, _color_property_set, ["greenOffset"]);
            Tweener.registerSpecialProperty("_color_blueMultiplier", _color_property_get, _color_property_set, ["blueMultiplier"]);
            Tweener.registerSpecialProperty("_color_blueOffset", _color_property_get, _color_property_set, ["blueOffset"]);
            Tweener.registerSpecialProperty("_color_alphaMultiplier", _color_property_get, _color_property_set, ["alphaMultiplier"]);
            Tweener.registerSpecialProperty("_color_alphaOffset", _color_property_get, _color_property_set, ["alphaOffset"]);
            Tweener.registerSpecialPropertySplitter("_color", _color_splitter);
            Tweener.registerSpecialPropertySplitter("_colorTransform", _colorTransform_splitter);
            Tweener.registerSpecialProperty("_brightness", _brightness_get, _brightness_set, [false]);
            Tweener.registerSpecialProperty("_tintBrightness", _brightness_get, _brightness_set, [true]);
            Tweener.registerSpecialProperty("_contrast", _contrast_get, _contrast_set);
            Tweener.registerSpecialProperty("_hue", _hue_get, _hue_set);
            Tweener.registerSpecialProperty("_saturation", _saturation_get, _saturation_set, [false]);
            Tweener.registerSpecialProperty("_dumbSaturation", _saturation_get, _saturation_set, [true]);
            return;
        }// end function

        public static function _color_splitter(param1, param2:Array) : Array
        {
            var _loc_3:* = new Array();
            if (param1 == null)
            {
                _loc_3.push({name:"_color_redMultiplier", value:1});
                _loc_3.push({name:"_color_redOffset", value:0});
                _loc_3.push({name:"_color_greenMultiplier", value:1});
                _loc_3.push({name:"_color_greenOffset", value:0});
                _loc_3.push({name:"_color_blueMultiplier", value:1});
                _loc_3.push({name:"_color_blueOffset", value:0});
            }
            else
            {
                _loc_3.push({name:"_color_redMultiplier", value:0});
                _loc_3.push({name:"_color_redOffset", value:AuxFunctions.numberToR(param1)});
                _loc_3.push({name:"_color_greenMultiplier", value:0});
                _loc_3.push({name:"_color_greenOffset", value:AuxFunctions.numberToG(param1)});
                _loc_3.push({name:"_color_blueMultiplier", value:0});
                _loc_3.push({name:"_color_blueOffset", value:AuxFunctions.numberToB(param1)});
            }
            return _loc_3;
        }// end function

        public static function _colorTransform_splitter(param1:Object, param2:Array) : Array
        {
            var _loc_3:* = new Array();
            if (param1 == null)
            {
                _loc_3.push({name:"_color_redMultiplier", value:1});
                _loc_3.push({name:"_color_redOffset", value:0});
                _loc_3.push({name:"_color_greenMultiplier", value:1});
                _loc_3.push({name:"_color_greenOffset", value:0});
                _loc_3.push({name:"_color_blueMultiplier", value:1});
                _loc_3.push({name:"_color_blueOffset", value:0});
            }
            else
            {
                _loc_3.push({name:"_color_redMultiplier", value:param1.redMultiplier});
                _loc_3.push({name:"_color_redOffset", value:param1.redOffset});
                _loc_3.push({name:"_color_blueMultiplier", value:param1.blueMultiplier});
                _loc_3.push({name:"_color_blueOffset", value:param1.blueOffset});
                _loc_3.push({name:"_color_greenMultiplier", value:param1.greenMultiplier});
                _loc_3.push({name:"_color_greenOffset", value:param1.greenOffset});
                _loc_3.push({name:"_color_alphaMultiplier", value:param1.alphaMultiplier});
                _loc_3.push({name:"_color_alphaOffset", value:param1.alphaOffset});
            }
            return _loc_3;
        }// end function

        public static function _oldColor_property_get(param1:Object, param2:Array, param3:Object = null) : Number
        {
            return param1.transform.colorTransform[param2[0]] * 100;
        }// end function

        public static function _oldColor_property_set(param1:Object, param2:Number, param3:Array, param4:Object = null) : void
        {
            var _loc_5:* = param1.transform.colorTransform;
            param1.transform.colorTransform[param3[0]] = param2 / 100;
            param1.transform.colorTransform = _loc_5;
            return;
        }// end function

        public static function _color_property_get(param1:Object, param2:Array, param3:Object = null) : Number
        {
            return param1.transform.colorTransform[param2[0]];
        }// end function

        public static function _color_property_set(param1:Object, param2:Number, param3:Array, param4:Object = null) : void
        {
            var _loc_5:* = param1.transform.colorTransform;
            param1.transform.colorTransform[param3[0]] = param2;
            param1.transform.colorTransform = _loc_5;
            return;
        }// end function

        public static function _brightness_get(param1:Object, param2:Array, param3:Object = null) : Number
        {
            var _loc_4:* = param2[0];
            var _loc_5:* = param1.transform.colorTransform;
            var _loc_6:* = 1 - (_loc_5.redMultiplier + _loc_5.greenMultiplier + _loc_5.blueMultiplier) / 3;
            var _loc_7:* = (_loc_5.redOffset + _loc_5.greenOffset + _loc_5.blueOffset) / 3;
            if (_loc_4)
            {
                return _loc_7 > 0 ? (_loc_7 / 255) : (-_loc_6);
            }
            else
            {
                return _loc_7 / 100;
            }
        }// end function

        public static function _brightness_set(param1:Object, param2:Number, param3:Array, param4:Object = null) : void
        {
            var _loc_6:Number = NaN;
            var _loc_7:Number = NaN;
            var _loc_5:* = param3[0];
            if (param3[0])
            {
                _loc_6 = 1 - Math.abs(param2);
                _loc_7 = param2 > 0 ? (Math.round(param2 * 255)) : (0);
            }
            else
            {
                _loc_6 = 1;
                _loc_7 = Math.round(param2 * 100);
            }
            var _loc_8:* = new ColorTransform(_loc_6, _loc_6, _loc_6, 1, _loc_7, _loc_7, _loc_7, 0);
            param1.transform.colorTransform = _loc_8;
            return;
        }// end function

        public static function _saturation_get(param1:Object, param2:Array, param3:Object = null) : Number
        {
            var _loc_4:* = getObjectMatrix(param1);
            var _loc_5:* = param2[0];
            var _loc_6:* = param2[0] ? (1 / 3) : (LUMINANCE_R);
            var _loc_7:* = _loc_5 ? (1 / 3) : (LUMINANCE_G);
            var _loc_8:* = _loc_5 ? (1 / 3) : (LUMINANCE_B);
            var _loc_9:* = ((_loc_4[0] - _loc_6) / (1 - _loc_6) + (_loc_4[6] - _loc_7) / (1 - _loc_7) + (_loc_4[12] - _loc_8) / (1 - _loc_8)) / 3;
            var _loc_10:* = 1 - (_loc_4[1] / _loc_7 + _loc_4[2] / _loc_8 + _loc_4[5] / _loc_6 + _loc_4[7] / _loc_8 + _loc_4[10] / _loc_6 + _loc_4[11] / _loc_7) / 6;
            return (_loc_9 + _loc_10) / 2;
        }// end function

        public static function _saturation_set(param1:Object, param2:Number, param3:Array, param4:Object = null) : void
        {
            var _loc_5:* = param3[0];
            var _loc_6:* = param3[0] ? (1 / 3) : (LUMINANCE_R);
            var _loc_7:* = _loc_5 ? (1 / 3) : (LUMINANCE_G);
            var _loc_8:* = _loc_5 ? (1 / 3) : (LUMINANCE_B);
            var _loc_9:* = param2;
            var _loc_10:* = 1 - _loc_9;
            var _loc_11:* = _loc_6 * _loc_10;
            var _loc_12:* = _loc_7 * _loc_10;
            var _loc_13:* = _loc_8 * _loc_10;
            var _loc_14:Array = [_loc_11 + _loc_9, _loc_12, _loc_13, 0, 0, _loc_11, _loc_12 + _loc_9, _loc_13, 0, 0, _loc_11, _loc_12, _loc_13 + _loc_9, 0, 0, 0, 0, 0, 1, 0];
            setObjectMatrix(param1, _loc_14);
            return;
        }// end function

        public static function _contrast_get(param1:Object, param2:Array, param3:Object = null) : Number
        {
            var _loc_5:Number = NaN;
            var _loc_6:Number = NaN;
            var _loc_4:* = param1.transform.colorTransform;
            _loc_5 = (param1.transform.colorTransform.redMultiplier + _loc_4.greenMultiplier + _loc_4.blueMultiplier) / 3 - 1;
            _loc_6 = (_loc_4.redOffset + _loc_4.greenOffset + _loc_4.blueOffset) / 3 / -128;
            return (_loc_5 + _loc_6) / 2;
        }// end function

        public static function _contrast_set(param1:Object, param2:Number, param3:Array, param4:Object = null) : void
        {
            var _loc_5:Number = NaN;
            var _loc_6:Number = NaN;
            _loc_5 = param2 + 1;
            _loc_6 = Math.round(param2 * -128);
            var _loc_7:* = new ColorTransform(_loc_5, _loc_5, _loc_5, 1, _loc_6, _loc_6, _loc_6, 0);
            param1.transform.colorTransform = _loc_7;
            return;
        }// end function

        public static function _hue_get(param1:Object, param2:Array, param3:Object = null) : Number
        {
            var _loc_6:Number = NaN;
            var _loc_8:Number = NaN;
            var _loc_4:* = getObjectMatrix(param1);
            var _loc_5:Array = [];
            [][0] = {angle:-179.9, matrix:getHueMatrix(-179.9)};
            _loc_5[1] = {angle:180, matrix:getHueMatrix(180)};
            _loc_6 = 0;
            while (_loc_6 < _loc_5.length)
            {
                
                _loc_5[_loc_6].distance = getHueDistance(_loc_4, _loc_5[_loc_6].matrix);
                _loc_6 = _loc_6 + 1;
            }
            var _loc_7:Number = 15;
            _loc_6 = 0;
            while (_loc_6 < _loc_7)
            {
                
                if (_loc_5[0].distance < _loc_5[1].distance)
                {
                    _loc_8 = 1;
                }
                else
                {
                    _loc_8 = 0;
                }
                _loc_5[_loc_8].angle = (_loc_5[0].angle + _loc_5[1].angle) / 2;
                _loc_5[_loc_8].matrix = getHueMatrix(_loc_5[_loc_8].angle);
                _loc_5[_loc_8].distance = getHueDistance(_loc_4, _loc_5[_loc_8].matrix);
                _loc_6 = _loc_6 + 1;
            }
            return _loc_5[_loc_8].angle;
        }// end function

        public static function _hue_set(param1:Object, param2:Number, param3:Array, param4:Object = null) : void
        {
            setObjectMatrix(param1, getHueMatrix(param2));
            return;
        }// end function

        public static function getHueDistance(param1:Array, param2:Array) : Number
        {
            return Math.abs(param1[0] - param2[0]) + Math.abs(param1[1] - param2[1]) + Math.abs(param1[2] - param2[2]);
        }// end function

        public static function getHueMatrix(param1:Number) : Array
        {
            var _loc_2:* = param1 * Math.PI / 180;
            var _loc_3:* = LUMINANCE_R;
            var _loc_4:* = LUMINANCE_G;
            var _loc_5:* = LUMINANCE_B;
            var _loc_6:* = Math.cos(_loc_2);
            var _loc_7:* = Math.sin(_loc_2);
            var _loc_8:Array = [_loc_3 + _loc_6 * (1 - _loc_3) + _loc_7 * (-_loc_3), _loc_4 + _loc_6 * (-_loc_4) + _loc_7 * (-_loc_4), _loc_5 + _loc_6 * (-_loc_5) + _loc_7 * (1 - _loc_5), 0, 0, _loc_3 + _loc_6 * (-_loc_3) + _loc_7 * 0.143, _loc_4 + _loc_6 * (1 - _loc_4) + _loc_7 * 0.14, _loc_5 + _loc_6 * (-_loc_5) + _loc_7 * -0.283, 0, 0, _loc_3 + _loc_6 * (-_loc_3) + _loc_7 * (-(1 - _loc_3)), _loc_4 + _loc_6 * (-_loc_4) + _loc_7 * _loc_4, _loc_5 + _loc_6 * (1 - _loc_5) + _loc_7 * _loc_5, 0, 0, 0, 0, 0, 1, 0];
            return [_loc_3 + _loc_6 * (1 - _loc_3) + _loc_7 * (-_loc_3), _loc_4 + _loc_6 * (-_loc_4) + _loc_7 * (-_loc_4), _loc_5 + _loc_6 * (-_loc_5) + _loc_7 * (1 - _loc_5), 0, 0, _loc_3 + _loc_6 * (-_loc_3) + _loc_7 * 0.143, _loc_4 + _loc_6 * (1 - _loc_4) + _loc_7 * 0.14, _loc_5 + _loc_6 * (-_loc_5) + _loc_7 * -0.283, 0, 0, _loc_3 + _loc_6 * (-_loc_3) + _loc_7 * (-(1 - _loc_3)), _loc_4 + _loc_6 * (-_loc_4) + _loc_7 * _loc_4, _loc_5 + _loc_6 * (1 - _loc_5) + _loc_7 * _loc_5, 0, 0, 0, 0, 0, 1, 0];
        }// end function

        private static function getObjectMatrix(param1:Object) : Array
        {
            var _loc_2:Number = 0;
            while (_loc_2 < param1.filters.length)
            {
                
                if (param1.filters[_loc_2] is ColorMatrixFilter)
                {
                    return param1.filters[_loc_2].matrix.concat();
                }
                _loc_2 = _loc_2 + 1;
            }
            return [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0];
        }// end function

        private static function setObjectMatrix(param1:Object, param2:Array) : void
        {
            var _loc_6:ColorMatrixFilter = null;
            var _loc_3:* = param1.filters.concat();
            var _loc_4:Boolean = false;
            var _loc_5:Number = 0;
            while (_loc_5 < _loc_3.length)
            {
                
                if (_loc_3[_loc_5] is ColorMatrixFilter)
                {
                    _loc_3[_loc_5].matrix = param2.concat();
                    _loc_4 = true;
                }
                _loc_5 = _loc_5 + 1;
            }
            if (!_loc_4)
            {
                _loc_6 = new ColorMatrixFilter(param2);
                _loc_3[_loc_3.length] = _loc_6;
            }
            param1.filters = _loc_3;
            return;
        }// end function

    }
}
